function(input, output) {
   
   output$distPlot <- renderPlot({
	pos = input$pos_select
	stat = input$statistic
	search_string = paste("SELECT * FROM luyia_pos WHERE part_of_speech='", pos, "' AND stat='", stat, "';")
    dat = dbGetQuery(con, search_string)
	ggplot(dat, aes(x=value, fill=language)) + 
	geom_histogram(binwidth=.02) + 
	facet_grid(split~language)
   })
}
